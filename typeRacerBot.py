from selenium import webdriver
from webdriver_manager.chrome import ChromeDriverManager
from pynput.keyboard import Key, Controller
from time import sleep
from PIL import ImageGrab
from pytesseract import image_to_string


def get_image():
    ss = ImageGrab.grab(bbox=(274, 475, 1000, 815))
    ss.save('test.jpg')


def get_content():
    x = image_to_string('test.jpg').replace("\n", " ").replace("|", "I").replace("Te. Wegyere?","")


    for c in x:
        if ord("a") <= ord(c) <= ord("z") or ord("A") <= ord(c) <= ord("Z"):
            break
        x.replace(c, "")
    return x


def practice():
    kboard = Controller()
    kboard.press(Key.ctrl_l)
    kboard.press(Key.alt_l)
    kboard.press("o")
    kboard.release(Key.ctrl_l)
    kboard.release(Key.alt_l)
    kboard.release("o")


def real_world():
    kboard = Controller()
    kboard.press(Key.ctrl_l)
    kboard.press(Key.alt_l)
    kboard.press("i")
    kboard.release(Key.ctrl_l)
    kboard.release(Key.alt_l)
    kboard.release("i")
    sleep(5)


if __name__ == '__main__':
    driver = webdriver.Chrome(ChromeDriverManager().install())
    driver.get("https://play.typeracer.com")
    sleep(5)
    # practice()
    real_world()
    sleep(5)
    get_image()
    print(get_content())
    while True:
        try:
            if "a" <= get_content()[1] <= "z" or "A" <= get_content()[1] <= "A":
                for c in get_content():
                    driver.find_element_by_class_name("txtInput").send_keys(c)
                    sleep(0.05)
            else:
                for c in get_content()[1:]:
                    driver.find_element_by_class_name("txtInput").send_keys(c)
                    sleep(0.05)
        except:
            continue
