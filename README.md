# TypeRacer bot

Made by Tomislav Ignjatov

## Description

Selenium based TypeRacer bot which takes a screenshot of the rough estimation where the text should be and through pytesseract gets the text from the image and inputs the text in the text field as if we were typing them.

